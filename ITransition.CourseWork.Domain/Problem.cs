﻿using ITransition.CourseWork.Domain.Abstract;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ITransition.CourseWork.Domain
{
    public class Problem : SocialElement
    {
        public bool IsActive { get; protected set; }

        public List<string> Answers { get; protected set; }

        public List<User> UsersAttemptedSolving { get; protected set; }

        public bool Solve(User user, string userAnswer)
        {
            if (UsersAttemptedSolving.Contains(user))
            {
                throw new AccessDeniedException();
            }
            bool result = CheckAnswer(user, userAnswer);
            RememberUserAnswer(user, result);
            return result;
        }

        private void RememberUserAnswer(User user, bool result)
        {
            throw new NotImplementedException();
        }

        private bool CheckAnswer(User user, string userAnswer)
        {
            return this.Answers.Contains(userAnswer.ToLowerInvariant());
        }

        public IEnumerable<string> Tags { get; protected set; }

        public Problem(User author, string[] tags)
        {
            this.Author = author;
            this.IsActive = true;
            this.Tags = tags;
            this.Answers = new List<string>();
        }

        public void Deactivate(User byWhom)
        {
            if (this.Author.Equals(byWhom) || byWhom.IsAdmin)
            {
                this.IsActive = false;
            }
            else
            {
                throw new AccessDeniedException();
            }
        }

        #region [ Blocking overrides ]
        public override void Comment(User user, string commentText)
        {
            if (!IsActive)
                throw new BlockedException();
            base.Comment(user, commentText);
        }

        public override void Like(User user)
        {
            if (!IsActive)
                throw new BlockedException();
            base.Like(user);
        }

        public override void Dislike(User user)
        {
            if (!IsActive)
                throw new BlockedException();
            base.Dislike(user);
        }

        #endregion

        #region [ Inline exceptions ]

        public class BlockedException : Exception { }

        public class AccessDeniedException : Exception { }

        #endregion
    }


}
