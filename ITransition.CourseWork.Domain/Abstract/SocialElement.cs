﻿using System;
using System.Collections.Generic;

namespace ITransition.CourseWork.Domain.Abstract
{
    public abstract class SocialElement
    {
        #region [ Properties ]

        public virtual int ElementId { get; protected set; }

        public User Author { get; protected set; }

        public virtual int Likes { get; protected set; }

        public virtual int Dislikes { get; protected set; }

        public virtual IList<Comment> Discussion { get; protected set; }

        public virtual DateTime UpdatedAt { get; set; }

        public virtual IEnumerable<User> LikedUsers
        {
            get { return this.likedUsers; }
        }

        public virtual IEnumerable<User> DislikedUsers
        {
            get { return this.dislikedUsers; }
        }

        #endregion

        #region [ Likes and Dislikes Management ]

        private List<User> likedUsers = new List<User>();
        private List<User> dislikedUsers = new List<User>();

        public virtual void Like(User user)
        {
            this.ResetUserAttitude(user);
            this.likedUsers.Add(user);
            this.Likes++;
        }

        public virtual void Dislike(User user)
        {
            this.ResetUserAttitude(user);
            this.dislikedUsers.Add(user);
            this.Dislikes++;
        }

        private void ResetUserAttitude(User user)
        {
            if (this.likedUsers.Remove(user))
                this.Likes--;

            if (this.dislikedUsers.Remove(user))
                this.Dislikes--;
        }

        #endregion

        #region [ Comment Management ]

        public virtual void Comment(User user, string commentText)
        {
            var comment = new Comment()
            {
                Author = user,
                Text = commentText
            };

            this.Discussion.Add(comment);
        }

        #endregion

        public SocialElement()
        {
            Discussion = new List<Comment>();
            UpdatedAt = DateTime.Now;
        }
    }
}
