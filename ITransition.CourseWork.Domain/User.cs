﻿using ITransition.CourseWork.Domain.Abstract;
using System.Collections.Generic;

namespace ITransition.CourseWork.Domain
{
    public class User
    {
        #region [ User Identity and personal data ]

        public string UserEmail { get; set; }

        public bool IsAdmin { get; private set; }

        #endregion

        public User(string username)
        {
            this.UserEmail = username;
        }

        #region [ Basic user actions ]

        public void Like(SocialElement element)
        {
            element.Like(this);
        }

        public void Dislike(SocialElement element)
        {
            element.Dislike(this);
        }

        public void CommentOn(SocialElement element, string text)
        {
            element.Comment(this, text);
        }

        public void DeactivateProblem(Problem problem)
        {
            problem.Deactivate(this);
        }

        public bool SolveProblem(Problem problem, string answer) {
            return problem.Solve(this, answer);
        }

        #endregion


        public User Adminificate()
        {
            this.IsAdmin = true;
            return this;
        }
    }
}
