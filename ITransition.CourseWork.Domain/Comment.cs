﻿using ITransition.CourseWork.Domain.Abstract;
namespace ITransition.CourseWork.Domain
{
    public class Comment : SocialElement
    {
        public string Text { get; set; }

    }
}
