﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ITransition.CourseWork.Domain;

namespace ITransition.CourseWork.Tests
{
    [TestClass]
    public class DomainTests
    {
        private Problem problem;
        private User user;

        private string username = "TestUser";
        string[] tags = { "Sick", "Awesome", "Kewl" };
        string[] answers = { "42", "666", "13" };

        [TestInitialize]
        public void Setup()
        {
            this.user = new User(username);
            this.problem = new Problem(this.user, tags);
            foreach (var answer in this.answers)
            {
                this.problem.Answers.Add(answer);
            }
        }

        [TestMethod]
        public void UserCanLike()
        {
            this.user.Like(this.problem);
            Assert.AreEqual(1, this.problem.Likes);
        }

        [TestMethod]
        public void UserCanDislike()
        {
            this.user.Dislike(this.problem);
            Assert.AreEqual(1, this.problem.Dislikes);
        }

        [TestMethod]
        public void UserOnlyLikesOnce()
        {
            this.user.Like(this.problem);
            this.user.Like(this.problem);

            Assert.AreEqual(1, this.problem.Likes);
        }

        [TestMethod]
        public void UserOnlyDislikesOnce()
        {
            this.user.Dislike(this.problem);
            this.user.Dislike(this.problem);

            Assert.AreEqual(1, this.problem.Dislikes);
        }

        [TestMethod]
        public void UserCanOnlyLikeOrDislike()
        {
            this.user.Like(this.problem);
            this.user.Dislike(this.problem);

            Assert.AreEqual(1, this.problem.Dislikes);
            Assert.AreEqual(0, this.problem.Likes);
        }

        [TestMethod]
        public void CommentsHaveUserSignature()
        {
            string text = "TestText";
            this.user.CommentOn(this.problem, text);
            Comment comment = this.problem.Discussion.Where(c => c.Text.Equals(text)).FirstOrDefault();
            Assert.AreEqual(this.username, comment.Author.UserEmail);
        }

        [TestMethod]
        [ExpectedException(typeof(Problem.BlockedException))]
        public void UserCanBlockProblem()
        {
            try
            {
                this.user.DeactivateProblem(this.problem);
                this.user.CommentOn(this.problem, "test comment");
            }
            finally
            {
                Assert.AreEqual(0, this.problem.Discussion.Count);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(Problem.AccessDeniedException))]
        public void NonAuthorsCantBlockProblem()
        {
            User secondUser = new User("VasilyPupkin");
            secondUser.DeactivateProblem(this.problem);
        }

        [TestMethod]
        [ExpectedException(typeof(Problem.BlockedException))]
        public void AdminsCanBlockProblem()
        {
            User adminUser = new User("R2D2").Adminificate();

            try
            {
                adminUser.DeactivateProblem(this.problem);
                this.user.CommentOn(this.problem, "test comment");
            }
            finally
            {
                Assert.AreEqual(0, this.problem.Discussion.Count);
            }
        }

        [TestMethod]
        public void CanAddTagsOnCreation()
        {
            foreach (var tag in this.problem.Tags)
            {
                Assert.IsTrue(this.tags.Contains(tag));
            }

            foreach (var tag in this.tags)
            {
                Assert.IsTrue(this.problem.Tags.Contains(tag));
            }
        }

        [TestMethod]
        public void CanSolveProblem()
        {
            string answer = "42";
            
            Assert.AreEqual(this.answers.Contains(answer), this.user.SolveProblem(this.problem, answer)); 
        }

    }
}
